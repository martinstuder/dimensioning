<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_AT" sourcelanguage="en">
<context>
    <name>Dimensioning</name>
    <message>
        <location filename="../dimensioning.py" line="130"/>
        <source>&amp;Dimensioning</source>
        <translation>Bemaßung</translation>
    </message>
    <message>
        <location filename="../dimensioning.py" line="193"/>
        <source>Dimensioning</source>
        <translation>Bemaßung</translation>
    </message>
    <message>
        <location filename="../dimensioning.py" line="117"/>
        <source>Configure</source>
        <translation>Konfiguration</translation>
    </message>
    <message>
        <location filename="../dimensioning.py" line="141"/>
        <source>use snapping if possible</source>
        <translation>Verwenden Sie nach Möglichkeit die Einrastfunktionen</translation>
    </message>
</context>
<context>
    <name>DimensioningDialogBase</name>
    <message>
        <location filename="../dimensioning_dialog_base.ui" line="14"/>
        <source>Dimensioning</source>
        <translation>Bemaßung</translation>
    </message>
    <message>
        <location filename="../dimensioning_dialog_base.ui" line="42"/>
        <source>red</source>
        <translation type="obsolete">rot</translation>
    </message>
    <message>
        <location filename="../dimensioning_dialog_base.ui" line="55"/>
        <source>black</source>
        <translation type="obsolete">schwarz</translation>
    </message>
    <message>
        <location filename="../dimensioning_dialog_base.ui" line="50"/>
        <source>Offset in mm:</source>
        <translation>Offset in mm:</translation>
    </message>
    <message>
        <location filename="../dimensioning_dialog_base.ui" line="66"/>
        <source>Color</source>
        <translation>Farbe</translation>
    </message>
    <message>
        <location filename="../dimensioning_dialog_base.ui" line="79"/>
        <source>Dimensioningcolor:</source>
        <translation>Bemaßungsfarbe:</translation>
    </message>
</context>
</TS>
