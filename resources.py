# -*- coding: utf-8 -*-

# Resource object code
#
# Created by: The Resource Compiler for PyQt5 (Qt v5.11.2)
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore

qt_resource_data = b"\
\x00\x00\x00\xc2\
\x89\
\x50\x4e\x47\x0d\x0a\x1a\x0a\x00\x00\x00\x0d\x49\x48\x44\x52\x00\
\x00\x00\x18\x00\x00\x00\x18\x08\x06\x00\x00\x00\xe0\x77\x3d\xf8\
\x00\x00\x00\x89\x49\x44\x41\x54\x48\x4b\xed\x93\x51\x0e\xc0\x20\
\x08\x43\xf1\xfe\x87\x9e\x61\x19\x86\x81\x20\xe9\xe2\xd7\xdc\x8f\
\x33\xea\x2b\xd4\xda\x68\xf3\xd7\x36\xf3\xe9\x08\x2c\x1d\x3e\x16\
\xfd\xcc\xa2\xeb\xe9\x57\x2e\xde\xce\x97\x76\x10\x91\x63\xe8\x14\
\xe9\x45\x04\xce\x05\x38\xc6\x4c\x40\x2a\x45\x22\x2c\x02\x83\x11\
\x09\x20\x70\xdd\x01\xff\xdf\x8c\xac\x83\x8a\xe7\xd1\x9e\xc1\xb5\
\x17\xfa\x05\xaa\xcf\xbe\x04\x9c\x6f\x80\x4a\xc8\x60\x25\x5e\x94\
\xd1\xda\x56\xd1\x4a\xd3\x17\xc5\xb4\x02\x9e\xed\x71\xf1\x3e\x02\
\xd6\xa6\xd4\x22\xd4\xf7\xf4\x1c\xfa\x62\xcb\xc5\x74\x7e\x1a\x1c\
\x17\xa7\x5e\xbc\x49\x00\x00\x00\x00\x49\x45\x4e\x44\xae\x42\x60\
\x82\
"

qt_resource_name = b"\
\x00\x07\
\x07\x3b\xe0\xb3\
\x00\x70\
\x00\x6c\x00\x75\x00\x67\x00\x69\x00\x6e\x00\x73\
\x00\x0c\
\x04\xa6\x38\xe7\
\x00\x64\
\x00\x69\x00\x6d\x00\x65\x00\x6e\x00\x73\x00\x69\x00\x6f\x00\x6e\x00\x69\x00\x6e\x00\x67\
\x00\x08\
\x0a\x61\x5a\xa7\
\x00\x69\
\x00\x63\x00\x6f\x00\x6e\x00\x2e\x00\x70\x00\x6e\x00\x67\
"

qt_resource_struct_v1 = b"\
\x00\x00\x00\x00\x00\x02\x00\x00\x00\x01\x00\x00\x00\x01\
\x00\x00\x00\x00\x00\x02\x00\x00\x00\x01\x00\x00\x00\x02\
\x00\x00\x00\x14\x00\x02\x00\x00\x00\x01\x00\x00\x00\x03\
\x00\x00\x00\x32\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\
"

qt_resource_struct_v2 = b"\
\x00\x00\x00\x00\x00\x02\x00\x00\x00\x01\x00\x00\x00\x01\
\x00\x00\x00\x00\x00\x00\x00\x00\
\x00\x00\x00\x00\x00\x02\x00\x00\x00\x01\x00\x00\x00\x02\
\x00\x00\x00\x00\x00\x00\x00\x00\
\x00\x00\x00\x14\x00\x02\x00\x00\x00\x01\x00\x00\x00\x03\
\x00\x00\x00\x00\x00\x00\x00\x00\
\x00\x00\x00\x32\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\
\x00\x00\x01\x70\xee\x3a\x51\xcb\
"

qt_version = [int(v) for v in QtCore.qVersion().split('.')]
if qt_version < [5, 8, 0]:
    rcc_version = 1
    qt_resource_struct = qt_resource_struct_v1
else:
    rcc_version = 2
    qt_resource_struct = qt_resource_struct_v2

def qInitResources():
    QtCore.qRegisterResourceData(rcc_version, qt_resource_struct, qt_resource_name, qt_resource_data)

def qCleanupResources():
    QtCore.qUnregisterResourceData(rcc_version, qt_resource_struct, qt_resource_name, qt_resource_data)

qInitResources()
